The scripts are named in a fairly self-explanatory manner.
Please note that I converted the jupyter notebooks into R & python scripts under the scripts folder. For convenience, html versions of each of these notebooks are under the html folder.

1_create.ipynb creates the dataset

the notebooks with a 2_ prefix are dsecriptive statistics

3_linear_all.ipynb is the main notebook that generates the results
for model 1, being the best of a regression based or decision tree model
for a pooled store model, with one model for each item

3B_items_no_history takes a look at the items which do not appear in the
train dataset but were in the test dataset

3C_nn.ipynb is the notebook that is an exploration of the neural network model,
this is model 2 in the presentation. Did not complete this due to time constraints

4_submission.ipynb adds in the id that is required for the sample submission

5_create.sh collates all the csv files for submission

6_final.ipynb removes duplicates and sets unit_sales to zero for missing values. another script set unit_sales to zero where values were negative..
