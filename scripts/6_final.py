#!/usr/bin/env python
# coding: utf-8

# In[117]:


import pandas as pd

df = pd.read_csv("final/submission.csv")


# In[118]:


df = df.rename(columns={'unit_sales':'unit_sales2'})
print(df['id'].dtype)
print(len(df['id'].unique()))
print(df.shape)
#df = df[df['id'] != 'id']
df.shape


# In[119]:


t = df.groupby(['id']).size().reset_index(name='counts')
t['counts'].value_counts()


# In[120]:


t.loc[t['counts'] == 2]


# In[121]:


df[df['id'] == '125498375']


# In[122]:


df2 = df.groupby('id').first().reset_index()
df2.shape


# In[123]:


sdf = pd.read_csv("data/sample_submission.csv")
sdf['id'] = sdf['id'].astype('str')
print(sdf.shape)
sdf.head()


# In[124]:


m = pd.merge(sdf, df2, how='left', on='id')
m.shape


# In[125]:


print(m.isnull().sum())
m.tail()['unit_sales2'].isna()


# In[126]:


m['unit_sales2'].dtype


# In[127]:


import numpy as np
m['unit_sales'] = m['unit_sales2'].apply(lambda x: 0 if pd.isna(x) else x)
m.head()


# In[128]:


m.drop(columns=['unit_sales2'], inplace=True)
m.head()


# In[129]:


m.to_csv("final/final_submission.csv", index=False)


# In[130]:


m.shape


# In[133]:


m['unit_sales'] = m['unit_sales'].apply(lambda x: int(float(x)))


# In[134]:


m.to_csv("final/fs.csv", index=False)


# In[ ]:




