#!/usr/bin/env python
# coding: utf-8

# ### create data
# short script to read train.csv from 7z format from s3 bucket, and write to feather format 

# In[1]:


import pandas as pd


# In[3]:


import boto3

s3 = boto3.client('s3')
#https://sydneys3.s3.ap-southeast-2.amazonaws.com/items.csv
#s3.download_file('sydneys3', 'train.7z', 'data/train.7z')


# In[ ]:


get_ipython().system('pip install py7zr')
from py7zr import unpack_7zarchive
import shutil


# In[ ]:


shutil.register_unpack_format('7zip', ['.7z'], unpack_7zarchive)
shutil.unpack_archive('data/train.7z', './data')


# In[ ]:


train = pd.read_csv("data/train.csv")


# In[ ]:


train.head()


# In[ ]:


train.shape


# In[ ]:


df.to_feather("data/trainpy.feather")

