require(data.table)


xdat <- fread('data/train.csv')

dict1 <- xdat[, j=list(unit_sales = mean(unit_sales, na.rm = TRUE)), by = list(item_nbr, store_nbr)]
xtest <- fread('data/test.csv')

xfor <- merge(xtest, dict1, all.x = T, by = c('item_nbr', 'store_nbr'))
xfor <- xfor[ , c('id', 'unit_sales')]
xfor[is.na(unit_sales),unit_sales:=0]
xfor$unit_sales <- pmax(xfor$unit_sales,0)

write.csv(xfor, 'submission/mean.csv', row.names = F, quote = F)



