#!/usr/bin/env python
# coding: utf-8

# ### items without history
# - just take average of sales (last date) in item family for that store 
# - if no store, then for all stores and weight by store for last date
# - only 60 of 114 had values for family in items.csv

# In[1]:


import pandas as pd
df_train = pd.read_feather("data/trainpy.feather")
df = pd.read_csv("items/items_not_in_train.csv")
df.head()


# In[5]:


import glob, os
os.chdir("/home/ec2-user/SageMaker/coles/")
fail = []
for file in glob.glob("output/failed/*.csv"):
    item_nbr = file.split(".")[0].split("/")[2]
    fail.append(item_nbr)


# In[6]:


import numpy as np
np.array(fail)


# In[7]:


f = pd.DataFrame(fail, columns=["item_nbr"])
itemlist = pd.concat([df, f])


# In[12]:


import pandas as pd
pred_df = pd.DataFrame(columns = ['item_nbr','pred'])


# In[18]:


df_train.loc[df_train['item_nbr'] == 1335305,'unit_sales'].mean()


# In[19]:


for item in itemlist['item_nbr'].values:
    try:
        unit_sale = df_train.loc[df_train['item_nbr'] == item,'unit_sales'].mean()
        print(unit_sale)
        pred_df.append(pd.DataFrame([item, unit_sale]))
    except:
        pass                       


# In[16]:


pred_df


# In[21]:


items = pd.read_csv("data/items.csv")
items['family'].value_counts()


# In[22]:


m = pd.merge(itemlist, items, how='left', on='item_nbr')
m.head()


# In[23]:


print(m.shape)
m['family'].value_counts()


# In[25]:


print(m['family'].value_counts().sum())
print(m.shape)


# In[25]:


type(fail)


# In[30]:


df_train['item_nbr'].head()


# In[28]:


df_train['keep'] = df_train['item_nbr'].apply(lambda x: x in fail)


# In[29]:


df_train['keep'].value_counts()


# In[31]:


df['item_nbr']


# In[11]:


test = pd.read_csv("data/test.csv")


# In[13]:


item = 2123750
test[test['item_nbr'] == item]


# In[3]:





# In[ ]:




