#!/usr/bin/env python
# coding: utf-8

# # descriptive stats on data

# - it became clear from the feature engineering and trying to run a simple linear model that the size of the data is cumbersome
# 
# - moved from ml.m52xlarget to ml.m5.4xlarge
# 
# - the number of observations in the test data is 15 days
# - in the training it was 1687 days
# 
# 
# - we have established that the items have to be pooled in some way to account for the sparsity of observations for some items
#     * at least for all stores and likely by item grouping
#    
# - model is regression based models , gradient boosted decision trees according to learnings from kaggle forecasting competitions (Bojer and Meldgaard)
# 
# - neural networks
#     
#     

# # stratification for modelling
# - start with the simplest which is regression by item pooled across store
# - indicator for has the item been sold in this store previously
# 

# - given time constraints am going to run lstm neural network model 
# - with multioutput 
# -
# 
# reference: https://machinelearningmastery.com/multi-output-regression-models-with-python/\https://machinelearningmastery.com/deep-learning-models-for-multi-output-regression/
# # https://www.tensorflow.org/tutorials/structured_data/time_series
# # Chollet

# In[1]:


import pandas as pd
import numpy as np


# In[2]:


get_ipython().run_cell_magic('time', '', 'df_train = pd.read_feather("data/trainpy.feather")\ndf_test = pd.read_csv("data/test.csv") ')


# In[3]:


df_train.head()


# In[4]:


df_train[:10]


# In[5]:


dfmatrix = df_train.pivot_table(values='unit_sales', index='item_nbr', columns='store_nbr', aggfunc='count')


# In[6]:


print(dfmatrix.shape)
type(dfmatrix)
dfmatrix.to_csv("output/entries_by_items_stores.csv")


# In[ ]:


#sample = X_train.sample()
df_train[]


# In[11]:


train_items = df_train['item_nbr'].unique()
test_items = df_test['item_nbr'].unique()


# In[12]:


type(train_items)


# In[13]:


def intersection(lst1, lst2):
    return list(set(lst1) & set(lst2))
 


# In[22]:


items = intersection(train_items, test_items)
len(items)


# In[28]:


#https://realpython.com/python-sets/
items_not_in_train = list(set(test_items).difference(train_items))
items_not_in_test = list(set(train_items).difference(test_items))
print("items not in train:", len(items_not_in_train))
print("items not in test (ignore):", len(items_not_in_test))


# In[37]:


from random import sample
item = sample(items,1)[0]
item


# In[39]:


df_train_i = df_train[df_train['item_nbr'] == 1956004]
df_test_i = df_test[df_test['item_nbr'] == 1956004]
data = pd.concat([df_train_i, df_test_i])


# In[41]:


data.shape


# In[50]:


len(data['store_nbr'].unique())


# In[51]:


data


# In[ ]:




