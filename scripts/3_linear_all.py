#!/usr/bin/env python
# coding: utf-8

# # descriptive stats on data

# - it became clear from the feature engineering and trying to run a simple linear model that the size of the data is cumbersome
# 
# - moved from ml.m52xlarget to ml.m5.4xlarge
# 
# - the number of observations in the test data is 15 days
# - in the training it was 1687 days
# 
# 
# - we have established that the items have to be pooled in some way to account for the sparsity of observations for some items
#     * at least for all stores and likely by item grouping
#    
# - model is regression based models , gradient boosted decision trees according to learnings from kaggle forecasting competitions (Bojer and Meldgaard)
# 
# - neural networks
#     
#     

# # stratification for modelling
# - start with the simplest which is regression by item pooled across store
# - indicator for has the item been sold in this store previously
# 

# - given time constraints am going to run lstm neural network model 
# - with multioutput 
# -
# 
# reference: 
# - https://machinelearningmastery.com/multi-output-regression-models-with-python/ 
# - https://machinelearningmastery.com/deep-learning-models-for-multi-output-regression/
# - https://www.tensorflow.org/tutorials/structured_data/time_series
# Chollet

# In[58]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime 
import sklearn.linear_model as lm
from sklearn.tree import DecisionTreeRegressor


# In[59]:


get_ipython().run_cell_magic('time', '', 'df_train = pd.read_feather("data/trainpy.feather")\ndf_test = pd.read_csv("data/test.csv") ')


# In[60]:


df_train.head()


# In[61]:


train_items = df_train['item_nbr'].unique()
test_items = df_test['item_nbr'].unique()

def intersection(lst1, lst2):
    return list(set(lst1) & set(lst2))
 
items = intersection(train_items, test_items)
len(items)

#https://realpython.com/python-sets/
items_not_in_train = list(set(test_items).difference(train_items))
items_not_in_test = list(set(train_items).difference(test_items))
print("items not in train:", len(items_not_in_train))
print("items not in test (ignore):", len(items_not_in_test))


# In[62]:


pd.DataFrame(items, columns=['item_nbr']).to_csv("items/items_both.csv", index=False)
pd.DataFrame(items_not_in_train, columns=['item_nbr']).to_csv("items/items_not_in_train.csv", index=False)


# In[63]:


def create_data_v(data, lags, start, end, target_size, dt):
    x_data = []
    y_data = [] 
    index = [i for i in range(0, len(data))]

    for i in range(start, end):
        x_data.append(data[i-lags:i])
        y_data.append(data[i:i+target_size])
        
        if (i < start + 5) or (i >= end - 5):
          print("i: %s (%s), x: %s-%s, y: %s-%s" % (i, dt.index[index[i]], i-lags, i-1, i, i+target_size))

    print(f"start: {start}, end: {end}, lags: {lags}, target_size {target_size}")
    print("\n")

    return(np.array(x_data), np.array(y_data))

def create_wide_df(X_data, y_data, x_name, y_name):
    xt = pd.DataFrame()
    yt = pd.DataFrame()

    for i in range(0, X_data.shape[0]):
        xt = xt.append(pd.DataFrame(X_data[i].T))
        yt = yt.append(pd.DataFrame(y_data[i].T))

    print(f"{x_name}.shape: {xt.shape}, {y_name}.shape: {yt.shape}")
    print(f"number of rows (dates by stores): {xt.shape[0] * xt.shape[1]}")

    return(xt, yt)


# In[64]:


def save_results(ptf, dtref_dates):

    ptf = ptf.rename(columns={0:'t0',1:'t1',2:'t2',3:'t3',4:'t4',5:'t5',6:'t6',7:'t7',8:'t8',9:'t9',
                              10:'t10',11:'t11',12:'t12',13:'t13',14:'t14',15:'t15'})

    ptf['store'] = [i + 1 for i in range(0, ptf.shape[0])]

    dtref = pd.DataFrame(dtref_dates)
    dtref['dtindex'] = dtref.index

    rf = pd.wide_to_long(ptf, 't', i='store', j="dtindex")
    rf = rf.rename(columns={'t':'unit_sales'})
    rf.reset_index(level=['store','dtindex'], inplace=True)

    m = pd.merge(rf, dtref, how='left', on='dtindex')
    m.drop(columns='dtindex', inplace=True)
    m['item_nbr'] = item
    m.to_csv("output/%s.csv" % item, index=False)
    print(m.head())
    print(f"\ncsv for {item} saved")
    print("======================")
    print("\n")    
    return(m)


# In[65]:


def train_and_predict(df_train, df_test, item):
    
    df_train_i = df_train[df_train['item_nbr'] == item]
    df_test_i = df_test[df_test['item_nbr'] == item]

    print("df_train:", df_train_i.shape)
    data = pd.concat([df_train_i, df_test_i])

    print(f"number of stores in train, test: {len(df_train_i['store_nbr'].unique())}, {len(df_test_i['store_nbr'].unique())}")

    dt = data.pivot(values='unit_sales', index=['date'], columns='store_nbr')
    print(f"reshape data: {dt.shape}, rows: {dt.shape[0] * dt.shape[1]}")
    
    df = dt.fillna(0)
    df.index = pd.to_datetime(df.index)
    
    #plt.figure(figsize=(7,5))
    #plt.plot(df[1].index, df[1].values)
    
    cutoff = datetime.strptime('2017-08-16', "%Y-%m-%d")
    df1 = df.loc[df.index < cutoff, :]

    #keep last 50 rows for final prediction, test set for submission
    dfh = df1[-50:]

    dfv = df1.values
    print("exclude test final", dfv.shape)

    cutoff = datetime.strptime('2017-08-16', "%Y-%m-%d")
    dff = df.loc[df.index >= cutoff, :]

    dtref_dates = dff.index

    dfhf = pd.concat([dfh, dff])
    dff = dff.values

    print("taking a split of 80/20 for own train/test")
    split = int(len(dfv) * 0.8)

    # create data for X and y train 
    lags = 50
    start = lags
    end = split
    target_size = 16

    print("\n create train data")
    X_train, y_train = create_data_v(dfv, lags, start, end, target_size, dt)

    print(f"X_train: {X_train.shape}, y_train {y_train.shape}")

    print("\n create test data")
    start = split
    end = len(dfv) - target_size

    X_test, y_test = create_data_v(dfv, lags, start, end, target_size, dt)

    print(f"X_test: {X_test.shape}, y_test {y_test.shape}")

    print(f"X_train[0].shape[0]: {X_train[0].shape[0]}\n")

    print("create wide train df\n")
    xt, yt = create_wide_df(X_train, y_train, "xt", "yt")

    print("create wide test df\n")
    xtt, ytt = create_wide_df(X_test, y_test, "xtt", "ytt")
    
    # linear regression
    m1= lm.LinearRegression()
    m1.fit(xt, yt)

    preds = m1.predict(xtt)
    print(f"\n preds.shape: {preds.shape}")
    pdf = pd.DataFrame(preds)

    #np.mean(np.abs(pdf.values - ytt.values))
    eval1 = np.sqrt(np.mean(np.square(np.log(pdf.values + 1) - np.log(ytt.values + 1))))

    # decision tree
    m2 = DecisionTreeRegressor()
    m2.fit(xt, yt)

    preds = m2.predict(xtt)
    pdf = pd.DataFrame(preds)
    #np.mean(np.abs(pdf.values - ytt.values))

    eval2 = np.sqrt(np.mean(np.square(np.log(pdf.values + 1) - np.log(ytt.values + 1))))
    print(f"final test nrows: {len(dfhf)}, window/lags: {lags}")
    
    print("\n create test final data")
    lags = 50
    start = lags
    end = 51
    target_size = 16
    X_testf, y_testf = create_data_v(dfhf, lags, start, end, target_size, dt)
    print("\n create wide test final df")
    xtf, ytf = create_wide_df(X_testf, y_testf, "xtf", "ytf")

    if eval1 < eval2:
        predsf = m1.predict(xtf)
    else:
        predsf = m2.predict(xtf)

    print(predsf.shape)
    ptf = pd.DataFrame(predsf)
    print("\n")
    return(ptf, dtref_dates)    


# In[ ]:


#items
#item = 1956004
# 2089036
#for i, item in enumerate(items[:10]):

for i, item in enumerate(items[36:]):
    print(f"{i}: {item}")
    try: 
        ptf, dtref_dates = train_and_predict(df_train, df_test, item)
        m = save_results(ptf, dtref_dates)
    except:
        print("either X_train or X_test does not have obs")
        print(f"\ncsv for {item} saved")
        print("======================")
        print("\n")  
        m = pd.DataFrame()
        m.to_csv("output/failed/%s.csv" % item, index=False)


# In[ ]:




