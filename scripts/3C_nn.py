#!/usr/bin/env python
# coding: utf-8

# # descriptive stats on data

# - it became clear from the feature engineering and trying to run a simple linear model that the size of the data is cumbersome
# 
# - moved from ml.m52xlarget to ml.m5.4xlarge
# 
# - the number of observations in the test data is 15 days
# - in the training it was 1687 days
# 
# 
# - we have established that the items have to be pooled in some way to account for the sparsity of observations for some items
#     * at least for all stores and likely by item grouping
#    
# - model is regression based models , gradient boosted decision trees according to learnings from kaggle forecasting competitions (Bojer and Meldgaard)
# 
# - neural networks
#     
#     

# # stratification for modelling
# - start with the simplest which is regression by item pooled across store
# - indicator for has the item been sold in this store previously
# 

# - given time constraints am going to run lstm neural network model 
# - with multioutput 
# -
# 
# reference: 
# - https://machinelearningmastery.com/multi-output-regression-models-with-python/ 
# - https://machinelearningmastery.com/deep-learning-models-for-multi-output-regression/
# - https://www.tensorflow.org/tutorials/structured_data/time_series
# Chollet

# In[ ]:


get_ipython().system('pip install keras')
get_ipython().system('pip install tensorflow')


# In[243]:


import pandas as pd
import numpy as np


# In[244]:


get_ipython().run_cell_magic('time', '', '#df_train = pd.read_feather("data/trainpy.feather")\ndata = pd.read_csv("data.csv")\nholiday_events = pd.read_csv("data/holidays_events.csv")')


# In[245]:


hols = holiday_events[(holiday_events['locale_name'] == "Ecuador") & (~holiday_events['transferred'])]
hols.set_index('date', inplace=True)
hols.index = pd.to_datetime(hols.index)

hols['type'].value_counts()

workday = hols[hols['type'] == 'Work Day']
hols = hols[hols['type'] != 'Work Day']

print(hols.head())
workday.head()

workday.shape

data.head()

dt = data.pivot(values='unit_sales', index=['date'], columns='store_nbr')
dt = dt.fillna(0)
dt.index = pd.to_datetime(dt.index)

dt.shape

dt.columns = ["s" + str(s) for s in dt.columns.values.tolist()]
dt.head()


# In[246]:


d = pd.DataFrame(list(dt.index), columns=['date'])
d['workday'] = d['date'].apply(lambda x: 1 if x in workday.index else 0)
print("n workdays:", d['workday'].sum())
d['hols'] = d['date'].apply(lambda x: 1 if x in hols.index else 0)
print("n workdays:", d['hols'].sum())
d.set_index('date', inplace=True)
d.index = pd.to_datetime(d.index)
d

df = pd.merge(d, dt, how='left', on='date')
print(df.columns.tolist())
df.head()
type(df.index)


# In[247]:


dt.index = pd.to_datetime(dt.index)
wdf = pd.get_dummies(dt.index.weekday, prefix='w', drop_first=True)
wdf.index = dt.index
mdf = pd.get_dummies(dt.index.month, prefix='m', drop_first=True)
mdf.index =  dt.index

ddf = pd.concat([wdf, mdf], axis=1)
ddf


# In[248]:


def shift_series_in_df(df, series_names=[], days_delta=1):
   ret = pd.DataFrame(index=df.index.copy())   
   for sn in series_names:
        ret[f'{sn}_p_{np.abs(days_delta)}'] = df[sn].transform(lambda x: x.shift(days_delta))
   return ret

   
def stack_shifted_sales(df, days_deltas=[1]):
    series_names = ['s1', 's2', 's3', 's4', 's5', 's6', 's7', 's8', 's9', 's10', 
                  's11', 's12', 's13', 's14', 's15', 's16', 's17', 's18', 's19', 
                  's20', 's21', 's22', 's23', 's24', 's25', 's26', 's27', 's28', 's29', 
                  's30', 's31', 's32', 's33', 's34', 's35', 's36', 's37', 's38', 's39', 
                  's40', 's41', 's42', 's43', 's44', 's45', 's46', 's47', 's48', 's49', 
                  's50', 's51', 's52', 's53', 's54']

    dfs = [df.copy()]
    for delta in days_deltas:
        shifted = shift_series_in_df(df, series_names=series_names, days_delta=delta)
        dfs.append(shifted)
    return pd.concat(dfs, axis=1, sort=False, copy=False)        
    


# In[249]:


series_names = ['s1', 's2', 's3', 's4', 's5', 's6', 's7', 's8', 's9', 's10', 
                  's11', 's12', 's13', 's14', 's15', 's16', 's17', 's18', 's19', 
                  's20', 's21', 's22', 's23', 's24', 's25', 's26', 's27', 's28', 's29', 
                  's30', 's31', 's32', 's33', 's34', 's35', 's36', 's37', 's38', 's39', 
                  's40', 's41', 's42', 's43', 's44', 's45', 's46', 's47', 's48', 's49', 
                  's50', 's51', 's52', 's53', 's54']


# In[250]:


scaler = MinMaxScaler(feature_range=(0,1))

scaled_cols = scaler.fit_transform(dt[series_names])
dt[series_names] = scaled_cols


# In[251]:


dtsh = stack_shifted_sales(dt, days_deltas=[1])
print(dtsh.shape)
# drop first row, no prev vals
#dtsh.dropna(inplace=True)
print(dtsh.shape)
dtsh


# In[252]:


dtsh.columns.shape


# In[253]:


df0 = pd.concat([d, ddf, dtsh], axis=1)
print(df0.columns)
print(df0.shape)
#print(ddf1.shape)
#print(d.index.dtype)
#print(ddf.index.dtype)
#print(dt.index.dtype)
df0.columns.tolist()
df0.columns

sales_cols = [col for col in df0.columns if 's' in col and '_p_' not in col]
stacked_sales_cols = [col for col in df0.columns if '_p_' in col]
other_cols = [col for col in df0.columns if col not in set(sales_cols) and col not in set(stacked_sales_cols)]

sales_cols = sorted(sales_cols)
stacked_sales_cols = sorted(stacked_sales_cols)

new_cols = other_cols + stacked_sales_cols + sales_cols
new_cols
# In[254]:


df0.dropna(inplace=True)
df0


# In[255]:


df0.index


# In[256]:


from datetime import datetime
cutoff = datetime.strptime('2017-08-16', "%Y-%m-%d")
df_test = df0.loc[df0.index >= cutoff, :]
df_train = df0.loc[df0.index < cutoff, :]


# In[345]:


X_cols_stacked = [col for col in df_train.columns if 'p_' in col]
X_cols_caldata = [col for col in df_train.columns if 'w_' in col or 'm_' in col or 'workday' in col or 'hols' in col]
X_cols = X_cols_stacked + X_cols_caldata

X = df_train[X_cols]
X


# In[259]:


X_colset = set(X_cols)
#print(X_colset)
y_cols = [col for col in df0.columns if col not in X_colset]
print(y_cols)
y = df_train[y_cols]
y.columns


# In[260]:


X_train, X_valid, y_train, y_valid = train_test_split(X, y, test_size=0.2, shuffle=False)


# In[261]:


X_train_vals = X_train.values.reshape((X_train.shape[0], 1, X_train.shape[1]))
X_valid_vals = X_valid.values.reshape((X_valid.shape[0], 1, X_valid.shape[1]))


# In[263]:


from keras.models import Sequential, Model
from keras.layers import LSTM, Dense, Conv1D, Input, Dropout, AvgPool1D, Reshape, Concatenate


# In[264]:


basic_model = Sequential()
basic_model.add(LSTM(73, input_shape=(X_train_vals.shape[1], X_train_vals.shape[2])))
basic_model.add(Dense(54))
basic_model.compile(loss='mean_absolute_error', optimizer='adam')


# In[265]:


basic_history = basic_model.fit(
    X_train_vals, 
    y_train.values, 
    epochs=60, 
    batch_size=30,
    validation_data=(X_valid_vals, y_valid.values),
    verbose=2,
    shuffle=False
)


# In[266]:


import matplotlib.pyplot as plt

def plot_history(history):
    plt.plot(history.history['loss'], label='train')
    plt.plot(history.history['val_loss'], label='test')
    plt.legend()
    plt.show()


# In[267]:


plot_history(basic_history)


# In[325]:


def model_eval(model, X_test, y_test):
    """
    Evaluate (step-by-step) model predictions from X_test and return predictions and real values in comparable format.
    """
    # prepare data
    sales_x_cols = [col for col in X_test.columns if 'p' in col]
    print(sales_x_cols)
    sales_x_idxs = [X_test.columns.get_loc(col) for col in sales_x_cols]
    sales_y_cols = [col for col in y_test.columns if 's' in col]
    sales_y_idxs = [y_test.columns.get_loc(col) for col in sales_y_cols]
    n_samples = y_test.shape[0]
    y_pred = np.zeros(y_test.shape)
    # iterate
    x_next = X_test.iloc[0].values
    for i in range(0, n_samples):
        x_arr = np.array([x_next])
        x_arr = x_arr.reshape(x_arr.shape[0], 1, x_arr.shape[1])
        y_pred[i] = model.predict(x_arr)[0] # input for prediction must be 2d, output is immediately extracted from 2d to 1d
        try:
            x_next = X_test.iloc[i+1].values
            x_next[sales_x_idxs] = y_pred[i][sales_y_idxs]
        except IndexError:
            pass  # this happens on last iteration, and x_next does not matter anymore
    return y_pred, y_test.values


# In[326]:


#y_valid.columns
X_valid.columns


# In[327]:


y_pred_basic, y_real = model_eval(basic_model, X_valid, y_valid)


# In[328]:


X_valid.columns


# In[329]:


template_df = pd.concat([X_valid, y_valid], axis=1)
template_df['is_test'] = np.repeat(True, template_df.shape[0])


# In[330]:


def unscale(y_arr, scaler, template_df, series_names, toint=False):
    """
    Unscale array y_arr of model predictions, based on a scaler fitted 
    to template_df.
    """
    tmp = template_df.copy()
    tmp[y_cols] = pd.DataFrame(y_arr, index=tmp.index)
    tmp[series_names] = scaler.inverse_transform(tmp[series_names])
    if toint:
        return tmp[y_cols].astype(int)
    return tmp[y_cols]


# In[340]:


basic_pred = unscale(y_pred_basic, scaler, template_df, series_names, toint=True)
basic_pred


# In[341]:


real = unscale(y_real, scaler, template_df, series_names, toint=True)


# In[342]:


real


# In[343]:


np.sqrt(np.mean(np.nan_to_num(np.square(np.log(preds + 1) - np.log(real + 1)))))


# In[344]:


pd.DataFrame(preds)


# In[347]:


X_cols_stacked = [col for col in df_test.columns if 'p_' in col]
X_cols_caldata = [col for col in df_test.columns if 'w_' in col or 'm_' in col or 'workday' in col or 'hols' in col]
X_cols = X_cols_stacked + X_cols_caldata

X_test = df_test[X_cols]
X_test


# In[355]:


X_colset = set(X_cols)
print(X_colset)
y_cols = [col for col in df0.columns if col not in X_colset]
print(y_cols)
y_test = df_test[y_cols]
y_test.columns
y_test


# In[348]:


X_test_vals = X_test.values.reshape((X_test.shape[0], 1, X_test.shape[1]))


# In[349]:


X_test_vals


# In[357]:


sales_x_cols = [col for col in X_test.columns if 'p' in col]
print(sales_x_cols)
sales_x_idxs = [X_test.columns.get_loc(col) for col in sales_x_cols]
print(sales_x_idxs)


# In[358]:


sales_y_cols = [col for col in y_test.columns if 's' in col]
sales_y_idxs = [y_test.columns.get_loc(col) for col in sales_y_cols]
n_samples = y_test.shape[0]
y_pred = np.zeros(y_test.shape)
# iterate
x_next = X_test.iloc[0].values

for i in range(0, n_samples):
    x_arr = np.array([x_next])
    x_arr = x_arr.reshape(x_arr.shape[0], 1, x_arr.shape[1])
    y_pred[i] = model.predict(x_arr)[0] # input for prediction must be 2d, output is immediately extracted from 2d to 1d
    try:
        x_next = X_test.iloc[i+1].values
        x_next[sales_x_idxs] = y_pred[i][sales_y_idxs]
    except IndexError:
        pass  # this happens on last iteration, and x_next does not matter anymore
    


# In[362]:


pd.DataFrame(y_pred)


# In[ ]:




