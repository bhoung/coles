#!/usr/bin/env python
# coding: utf-8

# ## combine results for submission

# In[8]:


import pandas as pd
import numpy as np


# In[48]:


test_df = pd.read_csv("data/test.csv")
#test_df['unit_sales'] = 0
test_df


# In[88]:


import glob, os
os.chdir("/home/ec2-user/SageMaker/coles/")
itemlist = []
for file in glob.glob("output/*.csv"):
    item_nbr = file.split(".")[0].split("/")[1]
    itemlist.append(item_nbr)


# In[89]:


np.array(itemlist)


# In[66]:


#import pandas as pd
#sub_df = pd.DataFrame(columns = ['id','unit_sales'])


# In[90]:


#item = int(itemlist[2])
#2287 2003709 (864, 6)

for i, item in enumerate(itemlist[2287:]):    
    #item = itemlist[2287]
    fname = f"output/{item}.csv"
    df = pd.read_csv(fname)
    df.rename(columns={"store":"store_nbr"}, inplace=True)
    m = pd.merge(df, test_df, how='left', on=['date','item_nbr','store_nbr'])
    m[['id', 'unit_sales']].to_csv(f"final/{item}.csv", index=False)
    print(i, item, m.shape)


# In[ ]:


#sub_df.append(m[['id','unit_sales']])


# In[79]:





# In[80]:


sub_df


# In[43]:


test_df['item_nbr'].dtype


# In[44]:


test_df.loc[test_df['item_nbr'] == item, 'unit_sales']


# In[19]:


sdf = pd.read_csv("data/sample_submission.csv")
sdf


# In[ ]:




