---
title: "Untitled"
author: "Brendan Houng"
date: "18/10/2021"
output: html_document
---
knitr::opts_chunk$set(echo = TRUE)

install.packages("feather")

library(tidyverse)
library(feather)
library(ggplot2)
library(reticulate)

setwd("/home/ec2-user/SageMaker/coles")

test              <- read_csv("data/test.csv")
transactions      <- read_csv("data/transactions.csv")
stores            <- read_csv("data/stores.csv")
sample_submission <- read_csv("data/sample_submission.csv")
oil               <- read_csv("data/oil.csv")
holiday_events    <- read_csv("data/holidays_events.csv")
items             <- read_csv("data/items.csv")

#train <- read_csv("data/train.csv")
#write_feather(train, "data/train.feather")
train <- read_feather("data/train.feather")

print("oil")
glimpse(oil)
print("holiday_events")
glimpse(holiday_events)
print("items")
glimpse(items)

ggplot(oil) + geom_line(aes(x=date, y=dcoilwtico))

head(holiday_events)
table(holiday_events$locale_name)
hols <- holiday_events %>% filter(locale_name == "Ecuador")

hols %>% filter(date < as.Date('2013-01-01', "%Y-%m-%d"))

hols %>% filter(!transferred & date < as.Date('2013-01-01', "%Y-%m-%d"))

table(hols$type)
# work day is a negative - a usual weekend that becomes a work day, offsetting the bridge

items %>% head()
print(paste("no of items:", nrow(items)))

table(items$family) %>% data.frame() %>% arrange(-Freq)

items %>% filter(family %in% c("GROCERY I", "GROCERY II", "EGGS")) %>% count() %>% pull()

table(items$class)
table(items$perishable)

items %>% filter(family %in% c("GROCERY I", "GROCERY II", "EGGS")) %>% select(perishable) %>% table()

table(items$family, items$perishable)

head(train)

head(test)

head(sample_submission)

fig <- function(width, heigth){
 options(repr.plot.width = width, repr.plot.height = heigth)
 }

# item time series - store
fig(8,6)
train %>% filter(item_nbr == 96995) %>% 
  ggplot(.) + geom_point(aes(x=date, y=unit_sales))

# item time series - store
train %>% filter(item_nbr == 99197) %>% 
  ggplot(.) + geom_point(aes(x=date, y=unit_sales)) 

library(lubridate)
train$year <- year(train$date)

fig(8,6)
train %>% filter(item_nbr == 99197) %>% 
  ggplot(.) + geom_point(aes(x=date, y=unit_sales, colour=as.factor(year))) 

items %>% filter(item_nbr == 99197)

length(unique(train$item_nbr))

itemlist <- sample(unique(train$item_nbr), 10)

fig(8,6)
for (i in 1:length(itemlist)) {
  df <- train %>% filter(item_nbr == itemlist[i])
  p1 <- ggplot(df) + geom_point(aes(x=date, y=unit_sales, colour=as.factor(year))) 
  print(p1)
}
