---
title: "Untitled"
author: "Brendan Houng"
date: "18/10/2021"
output: html_document
---
knitr::opts_chunk$set(echo = TRUE)

install.packages("feather")

library(tidyverse)
library(feather)
library(ggplot2)
library(reticulate)

setwd("/home/ec2-user/SageMaker/coles")

test              <- read_csv("data/test.csv")
transactions      <- read_csv("data/transactions.csv")
stores            <- read_csv("data/stores.csv")
sample_submission <- read_csv("data/sample_submission.csv")

train <- read_csv("data/train.csv")
#write_feather(train, "data/train.feather")
#train <- read_feather("data/train.feather")

print("train")
glimpse(train)
print("test")
glimpse(test)
print("transactions")
glimpse(transactions)
print("stores")
glimpse(stores)
print("sample_submission")
glimpse(sample_submission)

#transactions per day by store may be useful?
min(test$date)
max(test$date)
max(test$date) -  min(test$date) + 1

min(train$date)
max(train$date)

max(train$date) - min(train$date) + 1

fig <- function(width, heigth){
 options(repr.plot.width = width, repr.plot.height = heigth)
 }

fig(10, 8)

transactions %>% group_by(date) %>% 
 summarise(transactions=sum(transactions)) %>%
  ggplot(.) + geom_line(aes(x=date,y=transactions)) + theme_classic()

fig(20, 40)
transactions %>% group_by(date, store_nbr) %>% 
 summarise(transactions=sum(transactions)) %>% 
  ggplot(.) + geom_line(aes(x=date, y=transactions)) + theme_classic() + facet_grid(store_nbr ~ .)

print(paste("number of stores:", length(unique(stores$store_nbr))))
print(paste("no of items in train dataset:", length(unique(train$item_nbr))))
print(paste("no of items in test dataset:", length(unique(test$item_nbr))))

fig(8, 6)

store_df <- transactions %>% group_by(store_nbr) %>% summarise(n_transaction = sum(transactions)) %>% data.frame() %>% arrange(n_transaction)
store_df %>% ggplot(.) + geom_histogram(aes(n_transaction/1000000))
store_df %>% ggplot(.) + geom_histogram(aes(n_transaction/1000000)) + scale_x_log10()

store_items_df <- train %>% group_by(store_nbr) %>% 
                    summarise(n_items=length(unique(item_nbr))) 

store_items_df <- store_items_df %>% mutate(n_item_cat = case_when(
                            n_items < 3000 ~ 1,
                            n_items >= 3000 & n_items < 3400 ~ 2,
                            n_items >= 3400 ~ 3))

store_items_df %>% head()

fig(8, 6)
store_items_df %>% ggplot(.) + geom_histogram(aes(n_items, group=n_item_cat, fill=as.factor(n_item_cat)))

store_items_df %>% data.frame() %>% arrange(-n_items) %>% head()

head(stores)
table(stores$type)
table(stores$cluster)
table(stores$state) %>% data.frame() %>% arrange(-Freq) %>% t()
table(stores$city) %>% data.frame() %>% arrange(-Freq) %>% t()
pinchinca, guyas, azuay are the most populous states as indicated by number of stores
quito, guayaquil are the most populous cities 
m <- stores %>% left_join(store_items_df, by=c("store_nbr"="store_nbr"))

print("n_item_cat and type")
table(m$n_item_cat, m$type)
print("state and type")
table(m$state, m$type)
print("state and n_item_cat")
table(m$state, m$n_item_cat)

print("city and type")
table(m$city, m$type)
print("city and n_item_cat")
table(m$city, m$n_item_cat)

table(m$city, m$state) 
#https://medium.com/@outside2SDs/an-overview-of-correlation-measures-between-categorical-and-continuous-variables-4c7f85610365
Goodman Kruskal’s lambda
Phi co-efficient (uses chi-squared statistic)
Cramer’s V (uses chi-squared statistic)
Tschuprow’s T (uses chi-squared statistic)
Contingency coefficient C (uses chi-squared statistic)
m %>% head()

storelist <- sample(unique(stores$store_nbr), 10)
storelist

fig(20, 8)
transactions %>% group_by(date, store_nbr) %>% 
 summarise(transactions=sum(transactions)) %>% left_join(., m, by=("store_nbr"="store_nbr")) %>% filter(store_nbr %in% storelist) %>%
  ggplot(.) + geom_line(aes(x=date, y=transactions, colour=as.factor(city))) + theme_classic() + facet_grid(store_nbr ~ .)

fig(20, 8)
transactions %>% group_by(date, store_nbr) %>% 
 summarise(transactions=sum(transactions)) %>% left_join(., m, by=("store_nbr"="store_nbr")) %>% filter(store_nbr %in% storelist) %>%
  ggplot(.) + geom_line(aes(x=date, y=transactions, colour=as.factor(n_item_cat))) + theme_classic() + facet_grid(store_nbr ~ .)


