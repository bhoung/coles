# list all text files excluding README.txt
cd final/
f=`find . -type f \( -iname "*.csv" ! -iname "README.txt" \)`
echo $f

touch ../submission/submission.csv

# append all files into data.txt
cat $f > ../submission/submission.csv

wc -l ../submission/submission.csv
