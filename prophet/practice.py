#!/usr/bin/env python
# coding: utf-8

# In[6]:


import platform
platform.python_version()


# In[7]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime 


# In[8]:


get_ipython().system('pip install pandas-profiling')


# In[9]:


import os
os.getcwd()


# In[10]:


df_train = pd.read_feather("../data/trainpy.feather")


# In[17]:


df_train.columns


# In[12]:


df_test = pd.read_csv("../data/test.csv") 
train_items = df_train['item_nbr'].unique()
test_items = df_test['item_nbr'].unique()

def intersection(lst1, lst2):
    return list(set(lst1) & set(lst2))
 
items = intersection(train_items, test_items)
print(len(items))

#https://realpython.com/python-sets/
items_not_in_train = list(set(test_items).difference(train_items))
items_not_in_test = list(set(train_items).difference(test_items))
print("items not in train:", len(items_not_in_train))
print("items not in test (ignore):", len(items_not_in_test))


# In[19]:


itemlist = df_test['item_nbr'].unique()
len(itemlist)


# In[20]:


from random import sample
items = sample(list(itemlist),3)


# In[21]:


item = items[0]


# In[22]:


hols = pd.read_csv("../data/holidays_events.csv")
hols


# In[23]:


hdf = hols[hols['locale_name'] == "Ecuador"]
hdf


# In[24]:


stores = pd.read_csv("../data/stores.csv")
stores['state'].value_counts()


# In[25]:


stores.describe()


# In[26]:


get_ipython().magic('matplotlib inline')
hist = stores.hist(bins=30, sharey=True, figsize=(10, 10))


# In[27]:


# Frequency tables for each categorical feature
for column in stores.select_dtypes(include=["object"]).columns:
    display(100 * pd.crosstab(index=stores[column], columns="% observations", normalize="columns"))


# In[146]:


item = items[1]
item


# In[147]:


data = df_train.loc[(df_train['item_nbr'] == item) ]


# In[148]:


data.head()


# In[49]:


from pandas_profiling import ProfileReport
data.profile_report()


# In[149]:


ts = data['unit_sales'].values


# In[150]:


split = 0.8 * len(ts)
split = int(split)

lags = 50
start = lags
end = split
target_size = 16


# In[151]:


def create_data_v(data, lags, start, end, target_size, dt):
    x_data = []
    y_data = [] 
    index = [i for i in range(0, len(data))]

    for i in range(start, end):
        x_data.append(data[i-lags:i])
        y_data.append(data[i:i+target_size])
        
        if (i < start + 5) or (i >= end - 5):
          print("i: %s (%s), x: %s-%s, y: %s-%s" % (i, dt.index[index[i]], i-lags, i-1, i, i+target_size))

    print(f"start: {start}, end: {end}, lags: {lags}, target_size {target_size}")
    print("\n")

    return(np.array(x_data), np.array(y_data))


# In[152]:


X_train, y_train = create_data_v(ts, 12, 12, split, 12, data)


# In[153]:


X_train.shape


# In[154]:


data['store_nbr'].value_counts()


# In[155]:


data.set_index('date', inplace=True)


# In[157]:


data['unit_sales'].plot()


# In[158]:


stores = data['store_nbr'].unique()
#len(stores)
stores = np.sort(stores)


# In[160]:


#data.set_index('date', inplace=True)
data.index = pd.to_datetime(data.index)


# In[162]:


index = 0
fig, axs = plt.subplots(5, 6, figsize = (20,20))

for ax in axs.flatten():
    if index < 29:
        caption = '{}: {}'.format(index, stores[index])
        #print(caption)
        #ax.set_axis_off()
        ax.plot(data[data['store_nbr'] == stores[index]].index, data.loc[data['store_nbr'] == stores[index], 'unit_sales'])
        ax.text(min(data[data['store_nbr'] == stores[index]].index), max(data.loc[data['store_nbr'] == stores[index],'unit_sales']), caption, fontsize=8, color='g')
        
    else:
        ax.set_axis_off()
    index = index + 1


# In[163]:


data.loc[data['store_nbr'] == 27,'unit_sales'].plot()


# In[164]:


data.loc[data['store_nbr'] == 274,:].index


# In[172]:


df = data['unit_sales'].resample('W').sum()


# In[178]:


dt = pd.DataFrame(df)


# In[ ]:


from datetime import datetime
#datetime.strptime(str(x)[:7], '%Y-%m')


# In[186]:


datetime.strptime('2017-08-01', '%Y-%m-%d')


# In[194]:


#df_train.set_index('date', inplace=True)


# In[199]:


data.columns


# In[203]:


data['onpromotion'].value_counts()
data['promo'] = data['onpromotion'].apply(lambda x : 100 if x else 0 )


# In[206]:


dfp = data['promo'].resample('W').mean()
dfp = pd.DataFrame(dfp)


# In[213]:


dfm = dt.join(dfp)


# In[214]:


dt_train = dfm.loc[dfm.index < datetime.strptime('2017-08-01', '%Y-%m-%d')]


# In[215]:


dt_train.tail()


# In[223]:


plt.style.use('seaborn')
plt.rcParams["figure.figsize"] = (10, 8)


# In[224]:


dt_train['unit_sales'].plot()


# In[227]:


dt_train.reset_index(inplace=True)
dt_train


# In[229]:


import statsmodels.api as sm


# In[238]:


dt_train.set_index('ds', inplace=True)


# In[239]:


decomposition = sm.tsa.seasonal_decompose(dt_train["y"], period = 52) 
figure = decomposition.plot()
plt.show()


# In[241]:


dt_train


# In[242]:


dt_train['lny'] = np.log1p(dt_train['y'])


# In[243]:


decomposition = sm.tsa.seasonal_decompose(dt_train["lny"], period = 52) 
figure = decomposition.plot()
plt.show()


# In[245]:


df = dt_train.rename(columns={'date':'ds', 'unit_sales':'y'})


# In[247]:


cutoff_date = '2014-12-31'
xtrain, xvalid  = df.loc[df.index <= cutoff_date], df.loc[(df.index > cutoff_date) & (df.index < '2015-03-31')]
print(xtrain.shape, xvalid.shape)


# In[254]:


get_ipython().system('pip install pystan')


# In[255]:


get_ipython().system('pip install fbprophet')


# In[253]:


from fbprophet import Prophet
from fbprophet.diagnostics import cross_validation, performance_metrics
from fbprophet.plot import plot_yearly, add_changepoints_to_plot


# In[ ]:


m = Prophet()
m.fit(xtrain)
future = m.make_future_dataframe(periods= 30)
forecast = m.predict(future)
fig = m.plot(forecast, figsize=(CFG.img_dim1, CFG.img_dim2), xlabel = '')
a = add_changepoints_to_plot(fig.gca(), m, forecast)


# In[ ]:




