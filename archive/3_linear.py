#!/usr/bin/env python
# coding: utf-8

# # descriptive stats on data

# - it became clear from the feature engineering and trying to run a simple linear model that the size of the data is cumbersome
# 
# - moved from ml.m52xlarget to ml.m5.4xlarge
# 
# - the number of observations in the test data is 15 days
# - in the training it was 1687 days
# 
# 
# - we have established that the items have to be pooled in some way to account for the sparsity of observations for some items
#     * at least for all stores and likely by item grouping
#    
# - model is regression based models , gradient boosted decision trees according to learnings from kaggle forecasting competitions (Bojer and Meldgaard)
# 
# - neural networks
#     
#     

# # stratification for modelling
# - start with the simplest which is regression by item pooled across store
# - indicator for has the item been sold in this store previously
# 

# - given time constraints am going to run lstm neural network model 
# - with multioutput 
# -
# 
# reference: 
# - https://machinelearningmastery.com/multi-output-regression-models-with-python/ 
# - https://machinelearningmastery.com/deep-learning-models-for-multi-output-regression/
# - https://www.tensorflow.org/tutorials/structured_data/time_series
# Chollet

# In[1]:


import pandas as pd
import numpy as np


# In[2]:


get_ipython().run_cell_magic('time', '', 'df_train = pd.read_feather("data/trainpy.feather")\ndf_test = pd.read_csv("data/test.csv") ')


# In[3]:


df_train.head()


# In[4]:


train_items = df_train['item_nbr'].unique()
test_items = df_test['item_nbr'].unique()

def intersection(lst1, lst2):
    return list(set(lst1) & set(lst2))
 
items = intersection(train_items, test_items)
len(items)

#https://realpython.com/python-sets/
items_not_in_train = list(set(test_items).difference(train_items))
items_not_in_test = list(set(train_items).difference(test_items))
print("items not in train:", len(items_not_in_train))
print("items not in test (ignore):", len(items_not_in_test))


# In[5]:


pd.DataFrame(items, columns=['item_nbr']).to_csv("items/items_both.csv", index=False)
pd.DataFrame(items_not_in_train, columns=['item_nbr']).to_csv("items/items_not_in_train.csv", index=False)


# In[6]:


from random import sample
item = sample(items,1)[0]
item


# In[7]:


df_train_i = df_train[df_train['item_nbr'] == 1956004]
df_test_i = df_test[df_test['item_nbr'] == 1956004]
print(df_train_i.shape)
data = pd.concat([df_train_i, df_test_i])


# In[8]:


data.shape
len(data['store_nbr'].unique())
data


# In[9]:


dt = data.pivot(values='unit_sales', index=['date'], columns='store_nbr')
dt.shape
dt.shape[0] * dt.shape[1]


# In[10]:


df = dt.fillna(0)
print(df.columns)
df.index = pd.to_datetime(df.index)
df


# In[11]:


import matplotlib.pyplot as plt
plt.figure(figsize=(7,5))


plt.plot(df[1].index, df[1].values)


# In[12]:


from datetime import datetime 
cutoff = datetime.strptime('2017-08-16', "%Y-%m-%d")
df = df.loc[df.index < cutoff, :]
dfv = df.values
print(dfv.shape)
dfv


# In[13]:


df


# In[14]:


"""
start_index: usually 0
end_index: Train Split
history_size: window (lags)
target_size: how far into future
"""


# In[15]:


dfv[50-50:50]


# In[16]:


def create_data_v(data, lags, start, end, target_size):
  x_data = []
  y_data = [] 
  index = [i for i in range(0, len(data))]

  for i in range(start, end):
    x_data.append(data[i-lags:i])
    y_data.append(data[i:i+target_size])
    if (i < start + 5) or (i >= end - 5):
      print("i: %s (%s), x: %s-%s, y: %s-%s" % (i, dt.index[index[i]], i-lags, i-1, i, i+target_size))

  print(f"start: {start}, end: {end}, lags: {lags}, target_size {target_size}")
  return(np.array(x_data), np.array(y_data))


# In[17]:


#taking a split of 80/20
split = int(len(dfv) * 0.8)
split


# In[18]:


# create data for X and y train 

lags = 50
start = lags
end = split
target_size = 15

X_train, y_train = create_data_v(dfv, lags, start, end, target_size)


# In[19]:


y_train.shape
X_train.shape


# In[20]:


# create data for X and y test sets

start = split
end = len(dfv) - target_size

X_test, y_test = create_data_v(dfv, lags, start, end, target_size)


# In[21]:


print(y_test.shape)
print(X_test.shape)


# In[22]:


import sklearn.linear_model as lm
m = lm.LinearRegression()


# In[23]:


print(X_train.shape[0])
X_train[0].shape[0]

print(y_train.shape)
print(X_train.shape)
#y_train.reshape(1, -1)[0].shape
print(X_test.shape)
print(y_test.shape)


# In[24]:


xt = pd.DataFrame()
yt = pd.DataFrame()

for i in range(0, X_train.shape[0]):
    xt = xt.append(pd.DataFrame(X_train[i].T))
    yt = yt.append(pd.DataFrame(y_train[i].T))

print(xt.shape)
print(yt.shape)


# In[25]:


xt.shape[0] * xt.shape[1]


# In[26]:


m.fit(xt, yt)


# In[27]:


xtt = pd.DataFrame()
ytt = pd.DataFrame()

predictions = list()

for i in range(0, X_test.shape[0]):
    xtt = xtt.append(pd.DataFrame(X_test[i].T))
    ytt = ytt.append(pd.DataFrame(y_test[i].T))

    
print(xtt.shape)
print(ytt.shape)

preds = m.predict(xtt)

preds


# In[28]:


print(preds.shape)
ytt.shape


# In[29]:


pdf = pd.DataFrame(preds)
pdf


# In[113]:


ytt


# In[31]:


np.mean(np.abs(pdf.values - ytt.values))


# In[32]:


from sklearn.tree import DecisionTreeRegressor

m = DecisionTreeRegressor()
m.fit(xt, yt)

preds = m.predict(xtt)

preds
pdf = pd.DataFrame(preds)
np.mean(np.abs(pdf.values - ytt.values))


# In[33]:


dlist = pd.DataFrame(df.index)
dlist.shape


# In[34]:


df.head()
df.columns
dfd = np.zeros(df.shape).astype(datetime)

dfd[0,0]
for i in range(0, df.shape[0]):
  for j in range(0, df.shape[1]):
    dfd[i,j] = str(dlist['date'][i])
dfd = pd.DataFrame(dfd) 
print(dfd.shape)
dfd.tail()


# In[35]:


def create_dates(data, lags, start, end, target_size):
  x_dates = []
  y_dates = []

  index = [i for i in range(0, len(data))]

  for i in range(start, end):
    x_dates.append(data[i-lags:i])
    y_dates.append(data[i:i+target_size])
    if (i < start + 5) or (i >= end - 5):
      print("i: %s (%s), x: %s-%s, y: %s-%s" % (i, dt.index[index[i]], i-lags, i-1, i, i+target_size))

  print(f"start: {start}, end: {end}, lags: {lags}, target_size {target_size}")
  return(np.array(x_dates), np.array(y_dates))


# In[36]:


# create dates for test set

start = split
end = len(dfv) - target_size

X_dates, y_dates = create_dates(dfd, lags, start, end, target_size)


# In[37]:


print(X_dates.shape)
print(y_dates.shape)
print(X_test.shape)
print(y_test.shape)


# In[38]:


pd.DataFrame(y_dates[0].T).head()


# In[39]:


pd.DataFrame(X_dates[0].T).head()


# In[40]:


xtd = pd.DataFrame()
ytd = pd.DataFrame()

for i in range(0, X_dates.shape[0]):
    xtd = xtd.append(pd.DataFrame(X_dates[i].T))
    ytd = ytd.append(pd.DataFrame(y_dates[i].T))

print(xtd.shape)
print(ytd.shape)
print(xtt.shape)
print(ytt.shape)


# In[41]:


print(xtt.shape[0])
ytt.tail()


# In[42]:


pd.DataFrame(preds).tail()


# yhatdf = pd.Dataframe(columns=['date','item_nbr','store_nbr'])
# 
# for r in range(ytt.shape[0]):
#     print((r % 54) + 1)
#     yhatdf[r] = [ytt[]

# In[57]:


ytd[0] == "2017-04-22 00:00:00"


# In[65]:


ytd.reset_index(inplace=True)


# In[70]:


ytd[53:55]


# In[214]:


from datetime import datetime
result = ytd.loc[(ytd[0] == "2017-04-22 00:00:00"),:]
result.rename(columns={'index':'store'}, inplace=True)
result


# In[109]:


pd.DataFrame(preds[result.index])


# In[141]:


ytt = pd.DataFrame(ytt).set_index(pdf.index)


# In[202]:


yttf = ytt.iloc[list(result.index.values),:]


# In[203]:


pdff = pd.DataFrame(preds[result.index])


# In[204]:


np.mean(np.abs(pdff.values - yttf.values))


# In[205]:


#df.rename(columns={"A": "a", "B": "c"})
yttf = yttf.rename(columns={0:'t0',1:'t1',2:'t2',3:'t3',4:'t4',5:'t5',6:'t6',7:'t7',8:'t8',9:'t9',10:'t10',11:'t11',12:'t12',13:'t13',14:'t14'})
yttf


# In[206]:


yttf['store'] = [i + 1 for i in range(0, yttf.shape[0])]
yttf


# In[278]:


rf = pd.wide_to_long(yttf, 't', i='store', j="dtindex")
rf = rf.rename(columns={'t':'unit_sales'})
rf.reset_index(level=['store','dtindex'], inplace=True)
rf


# In[279]:


dtref = pd.DataFrame(result.drop(columns='store')[0:1].loc[0])
dtref.rename(columns={0:"date"}, inplace=True)
dtref['date'] = dtref['date'].apply(lambda x: str(x)[:10])
dtref['dtindex'] = dtref.index
dtref


# In[273]:





# In[284]:


m = pd.merge(rf, dtref, how='left', on='dtindex')
m.drop(columns='dtindex', inplace=True)
m['item_nbr'] = item
m


# In[ ]:


rf.to_csv("output/result.csv")

