# import python packages
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime 
import sklearn.linear_model as lm
from sklearn.tree import DecisionTreeRegressor


# read in data
df_train = pd.read_feather("data/trainpy.feather")
df_test = pd.read_csv("data/test.csv")

train_items = df_train['item_nbr'].unique()
test_items = df_test['item_nbr'].unique()

def intersection(lst1, lst2):
    return list(set(lst1) & set(lst2))
 
items = intersection(train_items, test_items)
len(items)

items_not_in_train = list(set(test_items).difference(train_items))
items_not_in_test = list(set(train_items).difference(test_items))
print("items not in train:", len(items_not_in_train))
print("items not in test (ignore):", len(items_not_in_test))

pd.DataFrame(items, columns=['item_nbr']).to_csv("items/items_both.csv", index=False)
pd.DataFrame(items_not_in_train, columns=['item_nbr']).to_csv("items/items_not_in_train.csv", index=False)



from random import sample
item = sample(items,1)[0]

df_train_i = df_train[df_train['item_nbr'] == 1956004]
df_test_i = df_test[df_test['item_nbr'] == 1956004]




print(df_train_i.shape)
data = pd.concat([df_train_i, df_test_i])

data.shape
len(data['store_nbr'].unique())
data


dt = data.pivot(values='unit_sales', index=['date'], columns='store_nbr')
dt.shape
dt.shape[0] * dt.shape[1]


df = dt.fillna(0)
print(df.columns)
df.index = pd.to_datetime(df.index)
df


plt.figure(figsize=(7,5))
plt.plot(df[1].index, df[1].values)


cutoff = datetime.strptime('2017-08-16', "%Y-%m-%d")
df1 = df.loc[df.index < cutoff, :]

#keep last 50 rows for final prediction, test set for submission
dfh = df1[-50:]
dfh.tail()


dfv = df1.values
print(dfv.shape)
dfv


df1


from datetime import datetime 
cutoff = datetime.strptime('2017-08-16', "%Y-%m-%d")
dff = df.loc[df.index >= cutoff, :]
dff



dtref_dates = dff.index
dtref_dates


dfhf = pd.concat([dfh, dff])
print(dfhf.shape)
dfhf


dff = dff.values
print(dff.shape)
dff


"""
start_index: usually 0
end_index: Train Split
history_size: window (lags)
target_size: how far into future
"""


# inspect first value in for loop below
# data[i-lags:i]
dfv[50-50:50]


def create_data_v(data, lags, start, end, target_size):
  x_data = []
  y_data = [] 
  index = [i for i in range(0, len(data))]

  for i in range(start, end):
    x_data.append(data[i-lags:i])
    y_data.append(data[i:i+target_size])
    if (i < start + 5) or (i >= end - 5):
      print("i: %s (%s), x: %s-%s, y: %s-%s" % (i, dt.index[index[i]], i-lags, i-1, i, i+target_size))

  print(f"start: {start}, end: {end}, lags: {lags}, target_size {target_size}")
  return(np.array(x_data), np.array(y_data))


#taking a split of 80/20
split = int(len(dfv) * 0.8)
split


# create data for X and y train 

lags = 50
start = lags
end = split
target_size = 16

X_train, y_train = create_data_v(dfv, lags, start, end, target_size)


y_train.shape
X_train.shape


# create data for X and y test sets

start = split
end = len(dfv) - target_size

X_test, y_test = create_data_v(dfv, lags, start, end, target_size)


print(y_test.shape)
print(X_test.shape)


print(X_train.shape[0])
X_train[0].shape[0]

print(y_train.shape)
print(X_train.shape)
print(X_test.shape)
print(y_test.shape)

def create_wide_df(X_data, y_data)
    xt = pd.DataFrame()
    yt = pd.DataFrame()

    for i in range(0, X_data.shape[0]):
        xt = xt.append(pd.DataFrame(X_data[i].T))
        yt = yt.append(pd.DataFrame(y_data[i].T))

    print(f"xt.shape: {xt.shape}, yt.shape: {yt.shape}")
    return(xt, yt)

xt = pd.DataFrame()
yt = pd.DataFrame()

for i in range(0, X_train.shape[0]):
    xt = xt.append(pd.DataFrame(X_train[i].T))
    yt = yt.append(pd.DataFrame(y_train[i].T))

print(xt.shape)
print(yt.shape)

xt.shape[0] * xt.shape[1]


m1= lm.LinearRegression()
m1.fit(xt, yt)


xtt = pd.DataFrame()
ytt = pd.DataFrame()

predictions = list()

for i in range(0, X_test.shape[0]):
    xtt = xtt.append(pd.DataFrame(X_test[i].T))
    ytt = ytt.append(pd.DataFrame(y_test[i].T))

    
print(xtt.shape)
print(ytt.shape)

preds = m1.predict(xtt)

preds


print(preds.shape)
ytt.shape


pdf = pd.DataFrame(preds)
pdf


ytt


np.mean(np.abs(pdf.values - ytt.values))

eval1 = np.sqrt(np.mean(np.square(np.log(pdf.values + 1) - np.log(ytt.values + 1))))
eval1




m2 = DecisionTreeRegressor()
m2.fit(xt, yt)

preds = m2.predict(xtt)

preds
pdf = pd.DataFrame(preds)
np.mean(np.abs(pdf.values - ytt.values))



eval2 = np.sqrt(np.mean(np.square(np.log(pdf.values + 1) - np.log(ytt.values + 1))))
eval2


print(len(dfhf),lags)



# create data for X and y final test 

lags = 50
start = lags
end = 51
target_size = 16

X_testf, y_testf = create_data_v(dfhf, lags, start, end, target_size)


print(X_testf.shape)
print(y_testf.shape)


xtf = pd.DataFrame()
ytf = pd.DataFrame()

predictions = list()

for i in range(0, X_testf.shape[0]):
    xtf = xtf.append(pd.DataFrame(X_testf[i].T))
    
print("xtf shape:", xtf.shape)

if eval1 < eval2:
    predsf = m1.predict(xtf)
else:
    predsf = m2.predict(xtf)


print(predsf.shape)
ptf = pd.DataFrame(predsf)
ptf.tail()



ptf = ptf.rename(columns={0:'t0',1:'t1',2:'t2',3:'t3',4:'t4',5:'t5',6:'t6',7:'t7',8:'t8',9:'t9',
                          10:'t10',11:'t11',12:'t12',13:'t13',14:'t14',15:'t15'})
ptf.tail()


ptf['store'] = [i + 1 for i in range(0, ptf.shape[0])]
ptf.tail()


dtref = pd.DataFrame(dtref_dates)
dtref['dtindex'] = dtref.index
dtref


rf = pd.wide_to_long(ptf, 't', i='store', j="dtindex")
rf = rf.rename(columns={'t':'unit_sales'})
rf.reset_index(level=['store','dtindex'], inplace=True)
rf

m = pd.merge(rf, dtref, how='left', on='dtindex')
m.drop(columns='dtindex', inplace=True)
m['item_nbr'] = item
m.to_csv("output/%s.csv" % item, index=False)
m

